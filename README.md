# Designing and Building Awesome REST APIs That Get Used
---------------------------

This is my reveal.js presentation of the talk I gave at the [UCAR SEA14 Conference](https://sea.ucar.edu/conference/2014/).

Live version is available at [https://dl.dropboxusercontent.com/u/203259/apidesign/index.html#/](https://dl.dropboxusercontent.com/u/203259/apidesign/index.html#/)

## Abstract

Date and Time: 2014 April 8th @ 3:45pm

Location: CG1 Auditorium

Speaker: Rion Dooley

Everyone loves a great app, but how many of us have stopped to consider the role the supporting APIs played in making the app great. Your choice of APIs can make or break a project. And, as developers and service providers, the design and development of your own APIs can make or break the projects of the people attempting to leverage your resources. In this talk, we walk through what goes into designing and building an awesome REST API that gets used. We draw from our own experiences building dozens of APIs for the public and private sectors and use examples of popular APIs in use today to highlight what works and what doesn't.

## Speaker Bio

Rion Dooley is a research associate at the Texas Advanced Computing where he leads the Web and Cloud Services group. He earned his Ph.D. in CS from LSU in 2004 with the support of a Board of Regents Fellowship. He worked at LSU's Center of Computation and Technology for 2 years before moving to UT. Past projects include the development of science gateways such as GridChem, the TeraGrid mobile user portal, and the XSEDE user portal. Rion is currently the lead architect of the Agave API, and co-PI of the Distributed Web Security for Science Gateways project. He is also a senior participant in the Science Gateway Institute planning project. Rion's primary research interests include distributed systems, cloud infrastructure, and data management.
